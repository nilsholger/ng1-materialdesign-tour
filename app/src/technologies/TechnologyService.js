(function(){
    'use strict';

    angular.module('technologies').service('technologyService', ['$q', TechnologyService]);


    function TechnologyService($q) {
      var technologies = [
        {
          name: 'Just Angular',
          avatar: 'angular.svg',
          content: 'One Framework: Mobile and Desktop ...'
        },
        {
          name: 'Angular CLI',
          avatar: 'angular.svg',
          content: 'Create Your Dream App.'
        },
        {
          name: 'AngularJS',
          avatar: 'angular.svg',
          content: 'Superheroic JavaScript MVW Framework ...'
        },
        {
          name: 'Material Design Lite',
          avatar: 'android.svg',
          content: 'Fast Mobile Web Responsive Framework ...'
        },
        {
          name: 'Chromebooks',
          avatar: 'chrome.svg',
          content: 'Pure Google Chromium OS Cloud Laptops ...'
        },
        {
          name: 'Firebase',
          avatar: 'angular.svg',
          content: 'Cloud Hosting, Realtime Database, Analytics ...'
        }
      ];

      return {
        loadAllTechnologies: function() {
          return $q.when(technologies);
        }
      };
    }

})();

(function(){
'use strict';

angular.module('technologies').controller('TechnologyController', [
  'technologyService', '$mdBottomSheet', '$mdSidenav', '$log', TechnologyController
]);


function TechnologyController(technologyService, $mdBottomSheet, $mdSidenav){
  var self = this;

  self.selected = null;
  self.technologies = [];
  self.selectTechnology = selectTechnology;
  self.makeContact = makeContact;
  self.toggleList = toggleTechnologiesList;

  function toggleTechnologiesList(){
    $mdSidenav('left').toggle();
  }

  technologyService.loadAllTechnologies().then(function(technologies){
            self.technologies = [].concat(technologies);
            self.selected = technologies[0];
  });

        function selectTechnology(technology){
          self.selected = technology;
        }

      function makeContact(selectedTechnology){

          $mdBottomSheet.show({
            controllerAs: "vm",
            controller: ['$mdBottomSheet', VisitSheetController],
            templateUrl: './src/technologies/view/visitSheet.html',
            parent: angular.element(document.getElementById('content'))
          });

          function VisitSheetController($mdBottomSheet){
            this.technology = selectedTechnology;
            this.items = [
              { name: 'Phone', icon: 'phone', icon_url: 'assets/images/phone.svg'},
              { name: 'Twitter', icon: 'twitter', icon_url: 'assets/images/twitter.svg'},
              { name: 'Google+', icon: 'google_plus', icon_url: 'assets/images/google_plus.svg'},
              { name: 'Hangout', icon: 'hangouts', icon_url: 'assets/images/hangouts.svg'}
            ];
            this.contactTechnology = function(action){
              console.log(action.name);
              window.open('https://plus.google.com/u/0/communities/115368820700870330756');
              $mdBottomSheet.hide(action);
            };
          }
      }
}
})();
